require 'gilded_rose'
require 'spec_helper'

describe GildedRose do

  describe "#update_quality" do
    it "does not change the name" do
      items = [Item.new("foo", 0, 0)]
      GildedRose.new(items).update_quality()
      expect(items[0].name).to eq "foo"
    end
  end

  describe 'Aged Brie' do
    context 'Update Sell-In' do
      it 'subtracts 1 to the quality when sell-in null' do
        items = [Item.new("Aged Brie", 0, 50)]
        GildedRose.new(items).update_quality()
        expect(items[0].sell_in).to eq -1
        end
      it 'subtracts 1 to the quality when sell-in positive' do
        items = [Item.new("Aged Brie", 1, 50)]
        GildedRose.new(items).update_quality()
        expect(items[0].sell_in).to eq 0
        end
      it 'subtracts 1 to the quality when sell-in negative' do
        items = [Item.new("Aged Brie", -1, 5)]
        GildedRose.new(items).update_quality()
        expect(items[0].sell_in).to eq -2
      end
    end
    context 'Update Quality' do
      context 'when the quality is positive or null and under 50' do
        context 'when sell-in is negative or null' do
          it 'add 2 to the items quality when quality is 0' do
            items = [Item.new("Aged Brie", 0, 0)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 2
          end
          it 'add 2 to the items quality when quality less than 49' do
            items = [Item.new("Aged Brie", -2, 24)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 26
          end
          it 'add 1 to the items quality when quality is 49' do
            items = [Item.new("Aged Brie", 0, 49)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 50
          end
        end
        context 'when sell-in is positive' do
          it 'add 1 to the items quality when quality is 0' do
            items = [Item.new("Aged Brie", 2, 0)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 1
          end
          it 'add 1 to the items quality when quality less than 50' do
            items = [Item.new("Aged Brie", 3, 24)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 25
          end
        end
      end
      context 'when the quality is 50' do
        context 'when sell-in is negative or null' do
          it 'keeps the quality when sell-in is 0' do
            items = [Item.new("Aged Brie", 0, 50)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 50
          end
          it 'keeps the quality when quality when sell-in is negative' do
            items = [Item.new("Aged Brie", -1, 50)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 50
          end
        end
        context 'when sell-in is positive' do
          it 'keeps the quality to 50' do
            items = [Item.new("Aged Brie", 3, 50)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 50
          end
        end
      end
      context 'when the quality is negative' do
        context 'when sell-in is negative or null' do
          it 'add 2 to the items quality when sell-in is null' do
            items = [Item.new("Aged Brie", 0, -1)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 1
            end
          it 'add 2 to the items quality when sell-in is negative' do
            items = [Item.new("Aged Brie", -1, -3)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq -1
          end
        end
        context 'when sell-in is positive' do
          it 'add 1 to the items quality when quality is 0' do
            items = [Item.new("Aged Brie", 2, -10)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq -9
          end
        end
      end
    end
  end
  describe 'Backstage passes to a TAFKAL80ETC concert' do
    context 'Update Sell-In' do
      it 'subtracts 1 to the quality when sell-in null' do
        items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 0, 50)]
        GildedRose.new(items).update_quality()
        expect(items[0].sell_in).to eq -1
      end
      it 'subtracts 1 to the quality when sell-in positive' do
        items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 1, 50)]
        GildedRose.new(items).update_quality()
        expect(items[0].sell_in).to eq 0
      end
      it 'subtracts 1 to the quality when sell-in negative' do
        items = [Item.new("Backstage passes to a TAFKAL80ETC concert", -1, 5)]
        GildedRose.new(items).update_quality()
        expect(items[0].sell_in).to eq -2
      end
    end
    context 'Update Quality' do
      context 'when the quality is positive and under 48' do
        context 'when sell-in is negative or null' do
          it 'sets the quality to O when sell-in null' do
            items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 0, 34)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 0
          end
          it 'sets the quality to O when sell-in negative' do
            items = [Item.new("Backstage passes to a TAFKAL80ETC concert", -3, 49)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 0
          end
        end
        context 'when sell-in is positive' do
          context 'when sell-in equals or more than 11' do
            it 'add 1 to the items quality when quality is 11' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 11, 0)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 1
            end
            it 'add 1 to the items quality when quality is 15' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 24, 3)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 4
            end
          end
          context 'when sell-in is between 6 and 10' do
            it 'add 2 to the items quality when sell-in is 10' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 10, 0)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 2
            end
            it 'add 2 to the items quality when sell-in is 5' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 8, 3)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 5
            end
            it 'add 2 to the items quality when sell-in is 8' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 6, 1)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 3
            end
          end
          context 'when sell-in is between 1 and 5' do
            it 'add 3 to the items quality when quality is ' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 5, 0)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 3
            end
            it 'add 3 to the items quality when quality is 5' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 3, 3)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 6
            end
            it 'add 3 to the items quality when quality is 8' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 1, 2)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 5
            end
          end
        end
      end
      context 'when the quality is positive and is 48' do
        context 'when sell-in is negative or null' do
          it 'sets the quality to O when sell-in null' do
            items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 0, 48)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 0
          end
          it 'add 1 to the items quality when sell-in negative' do
            items = [Item.new("Backstage passes to a TAFKAL80ETC concert", -3, 48)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 0
          end
        end
        context 'when sell-in is positive' do
          context 'when sell-in equals or more than 11' do
            it 'add 1 to the items quality when sell-in is 11' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 11, 48)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 49
            end
            it 'add 1 to the items quality when sell-in is 24' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 24, 48)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 49
            end
          end
          context 'when sell-in is between 6 and 10' do
            it 'add 2 to the items quality when sell-in is 10' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 10, 48)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 50
            end
            it 'add 2 to the items quality when sell-in is 8' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 8, 48)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 50
            end
            it 'add 2 to the items quality when sell-in is 6' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 6, 48)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 50
            end
          end
          context 'when sell-in is between 1 and 5' do
            it 'add 2 to the items quality  when sell-in is 5' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 5, 48)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 50
            end
            it 'add 2 to the items quality  when sell-in is 3' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 3, 48)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 50
            end
            it 'add 2 to the items quality  when sell-in is 1' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 1, 48)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 50
            end
          end
        end
      end
      context 'when the quality is positive and is 49' do
        context 'when sell-in is negative or null' do
          it 'sets the quality to O when sell-in null' do
            items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 0, 49)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 0
          end
          it 'add 1 to the items quality when sell-in negative' do
            items = [Item.new("Backstage passes to a TAFKAL80ETC concert", -3, 49)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 0
          end
        end
        context 'when sell-in is positive' do
          context 'when sell-in equals or more than 11' do
            it 'add 1 to the items quality when quality is 15' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 24, 49)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 50
            end
          end
          context 'when sell-in is between 6 and 10' do
            it 'add 1 to the items quality when quality is 50' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 10, 49)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 50
            end
          end
          context 'when sell-in is between 1 and 5' do
            it 'add 1 to the items quality' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 5, 49)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 50
            end
          end
        end
      end
      context 'when the quality is 50' do
        context 'when sell-in is negative or null' do
          it 'sets the quality to O when sell-in null' do
            items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 0, 34)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 0
          end
          it 'add 1 to the items quality when sell-in negative' do
            items = [Item.new("Backstage passes to a TAFKAL80ETC concert", -3, 49)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 0
          end
        end
        context 'when sell-in is positive' do
          context 'when sell-in equals or more than 11' do
            it 'keeps the items quality when sell-in is 11' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 11, 50)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 50
            end
            it 'keeps the items quality when sell-in is 44' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 44, 50)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 50
            end
          end
          context 'when sell-in is between 6 and 10' do
            it 'keeps the items quality when quality is 10' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 10, 50)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 50
            end
            it 'keeps the items quality when quality is 7' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 7, 50)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 50
            end
            it 'keeps the items quality when quality is 6' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 6, 50)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 50
            end
          end
          context 'when sell-in is between 1 and 5' do
            it 'keeps the items quality when quality is 5' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 5, 50)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 50
            end
            it 'keeps the items quality when quality is 3' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 3, 50)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 50
            end
            it 'keeps the items quality when quality is 1' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 1, 50)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 50
            end
          end
        end
      end
      context 'when the quality is over 50' do
        context 'when sell-in is negative or null' do
          it 'sets the quality to 0' do
            items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 0,51)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 0
          end
          it 'sets the quality to 0' do
            items = [Item.new("Backstage passes to a TAFKAL80ETC concert", -3, 52)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 0
          end
        end
        context 'when sell-in is positive' do
          context 'when sell-in equals or more than 11' do
            it 'keeps the items quality when sell-in is 11' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 11, 52)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 52
            end
            it 'keeps the items quality when sell-in is 44' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 44, 56)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 56
            end
          end
          context 'when sell-in is between 6 and 10' do
            it 'keeps the items quality when quality is 10' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 10, 57)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 57
            end
            it 'keeps the items quality when quality is 7' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 7, 51)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 51
            end
            it 'keeps the items quality when quality is 6' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 6, 52)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 52
            end
          end
          context 'when sell-in is between 1 and 5' do
            it 'keeps the items quality when quality is 5' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 5, 53)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 53
            end
            it 'keeps the items quality when quality is 3' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 3, 51)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 51
            end
            it 'keeps the items quality when quality is 1' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 1, 51)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 51
            end
          end
        end
      end
      context 'when the quality is negative' do
        context 'when sell-in is negative or null' do
          it 'sets the quality to O when sell-in null' do
            items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 0, -2)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 0
          end
          it 'add 1 to the items quality when sell-in negative' do
            items = [Item.new("Backstage passes to a TAFKAL80ETC concert", -3, -13)]
            GildedRose.new(items).update_quality()
            expect(items[0].quality).to eq 0
          end
        end
        context 'when sell-in is positive' do
          context 'when sell-in equals or more than 11' do
            it 'add 1 to the items quality when sell-in 11' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 11, -4)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq -3
            end
            it 'add 1 to the items quality when sell-in is 15' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 24, -1)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 0
            end
          end
          context 'when sell-in is between 6 and 10' do
            it 'add 2 to the items quality when sell-in is 10' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 10, -2)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq 0
            end
            it 'add 2 to the items quality sell-in is 5' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 8, -4)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq -2
            end
            it 'add 2 to the items quality when sell-in is 6' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 6, -4)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq -2
            end
          end
          context 'when sell-in is between 1 and 5' do
            it 'add 3 to the items quality sell-in is 5 ' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 5, -13)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq -10
            end
            it 'add 3 to the items quality when sell-in is 3' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 3, -7)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq -4
            end
            it 'add 3 to the items quality when sell-in is 1' do
              items = [Item.new("Backstage passes to a TAFKAL80ETC concert", 1, -4)]
              GildedRose.new(items).update_quality()
              expect(items[0].quality).to eq -1
            end
          end
        end
      end
    end
  end
  describe 'Sulfuras, Hand of Ragnaros' do
    context 'Update Sell-In' do
      it 'subtracts 1 to the quality when sell-in null' do
        items = [Item.new("Sulfuras, Hand of Ragnaros", 0, 50)]
        GildedRose.new(items).update_quality()
        expect(items[0].sell_in).to eq 0
      end
      it 'subtracts 1 to the quality when sell-in positive' do
        items = [Item.new("Sulfuras, Hand of Ragnaros", 1, 50)]
        GildedRose.new(items).update_quality()
        expect(items[0].sell_in).to eq 1
      end
      it 'subtracts 1 to the quality when sell-in negative' do
        items = [Item.new("Sulfuras, Hand of Ragnaros", -1, 5)]
        GildedRose.new(items).update_quality()
        expect(items[0].sell_in).to eq -1
      end
    end
    describe 'Update quality' do
      describe 'when quality is positive' do
        it 'keeps the quality when sell_in negative' do
          items = [Item.new("Sulfuras, Hand of Ragnaros", -1, 50)]
          GildedRose.new(items).update_quality()
          expect(items[0].quality).to eq 50
        end
        it 'keeps the quality when sell_in is null' do
          items = [Item.new("Sulfuras, Hand of Ragnaros", 0, 1)]
          GildedRose.new(items).update_quality()
          expect(items[0].quality).to eq 1
        end
        it 'keeps the quality when sell_in is positive' do
          items = [Item.new("Sulfuras, Hand of Ragnaros", 0, 12)]
          GildedRose.new(items).update_quality()
          expect(items[0].quality).to eq 12
        end

      end
      describe 'when quality is negative' do
        it 'sets the quality to 0 when sell_in negative' do
          items = [Item.new("Sulfuras, Hand of Ragnaros", -1, -1)]
          GildedRose.new(items).update_quality()
          expect(items[0].quality).to eq  -1
        end
        it 'sets the quality to 0 when sell_in is null' do
          items = [Item.new("Sulfuras, Hand of Ragnaros", 0, -23)]
          GildedRose.new(items).update_quality()
          expect(items[0].quality).to eq -23
        end
        it 'sets the quality to 0 when sell_in is positive' do
          items = [Item.new("Sulfuras, Hand of Ragnaros", 0, -3)]
          GildedRose.new(items).update_quality()
          expect(items[0].quality).to eq -3
        end

      end
      describe 'when quality is null' do
        it 'keeps the quality when sell_in negative' do
          items = [Item.new("Sulfuras, Hand of Ragnaros", -1, 0)]
          GildedRose.new(items).update_quality()
          expect(items[0].quality).to eq 0
        end
        it 'keeps the quality when sell_in is null' do
          items = [Item.new("Sulfuras, Hand of Ragnaros", 0, 0)]
          GildedRose.new(items).update_quality()
          expect(items[0].quality).to eq 0
        end
        it 'keeps the quality when sell_in is positive' do
          items = [Item.new("Sulfuras, Hand of Ragnaros", 0, 0)]
          GildedRose.new(items).update_quality()
          expect(items[0].quality).to eq 0
        end

      end
    end
  end
  describe 'Other object' do
    context 'Update sell_in' do
      it 'subtracts 1 to the sell_in when sell-in null' do
        items = [Item.new("Obj", 0, 50)]
        GildedRose.new(items).update_quality()
        expect(items[0].sell_in).to eq -1
      end
      it 'subtracts 1 to the quality when sell-in positive' do
        items = [Item.new("Obj", 1, 50)]
        GildedRose.new(items).update_quality()
        expect(items[0].sell_in).to eq 0
      end
      it 'subtracts 1 to the sell_in when sell-in negative' do
        items = [Item.new("Obj", -1, 5)]
        GildedRose.new(items).update_quality()
        expect(items[0].sell_in).to eq -2
      end
    end
    context 'Update quality' do
      context 'sell_in positive' do
        it 'substract 1 to the quality when quality is over 0' do
          items = [Item.new("Obj", 1, 3)]
          GildedRose.new(items).update_quality()
          expect(items[0].quality).to eq 2
        end
        it 'keeps the quality when quality is 0' do
          items = [Item.new("Obj", 1, 0)]
          GildedRose.new(items).update_quality()
          expect(items[0].quality).to eq 0
        end
        it 'keeps the quality when quality is negative' do
          items = [Item.new("Obj", 1, -1)]
          GildedRose.new(items).update_quality()
          expect(items[0].quality).to eq -1
        end
        it 'substract 1 the quality when quality is over 50' do
          items = [Item.new("Obj", 1, 56)]
          GildedRose.new(items).update_quality()
          expect(items[0].quality).to eq 55
        end
      end

    end
    context 'sell_in null' do
      it 'substract 1 to the quality when quality is 1 ' do
        items = [Item.new("Obj", 0, 1)]
        GildedRose.new(items).update_quality()
        expect(items[0].quality).to eq 0
      end
      it 'substract 2 to the quality when quality is over 1 ' do
        items = [Item.new("Obj", 0, 2)]
        GildedRose.new(items).update_quality()
        expect(items[0].quality).to eq 0
      end
      it 'keeps the quality when quality is 0' do
        items = [Item.new("Obj", 0, 0)]
        GildedRose.new(items).update_quality()
        expect(items[0].quality).to eq 0
      end
      it 'keeps the quality when quality is negative' do
        items = [Item.new("Obj",0, -1)]
        GildedRose.new(items).update_quality()
        expect(items[0].quality).to eq -1
      end
      it 'substract 2 the quality when quality is over 50' do
        items = [Item.new("Obj", 0, 56)]
        GildedRose.new(items).update_quality()
        expect(items[0].quality).to eq 54
      end
    end
    context 'sell_in is negative' do
      it 'substract 1 to the quality when quality is 1 ' do
        items = [Item.new("Obj", -1, 1)]
        GildedRose.new(items).update_quality()
        expect(items[0].quality).to eq 0
      end
      it 'substract 2 to the quality when quality is over 1 ' do
        items = [Item.new("Obj", -1, 2)]
        GildedRose.new(items).update_quality()
        expect(items[0].quality).to eq 0
      end
      it 'keeps the quality when quality is 0' do
        items = [Item.new("Obj", -1, 0)]
        GildedRose.new(items).update_quality()
        expect(items[0].quality).to eq 0
      end
      it 'keeps the quality when quality is negative' do
        items = [Item.new("Obj",-1, -1)]
        GildedRose.new(items).update_quality()
        expect(items[0].quality).to eq -1
      end
      it 'substract 2 the quality when quality is over 50' do
        items = [Item.new("Obj", -1, 56)]
        GildedRose.new(items).update_quality()
        expect(items[0].quality).to eq 54
      end
    end

  end
end
